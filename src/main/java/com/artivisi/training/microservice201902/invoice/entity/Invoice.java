package com.artivisi.training.microservice201902.invoice.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity @Data
public class Invoice {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne @JoinColumn(name = "id_customer")
    private Customer customer;

    @NotEmpty
    @Size(min = 5, max = 100)
    @Column(name = "invoice_number")
    private String number;

    @NotNull
    private LocalDate createDate;

    @NotEmpty @Size(min = 3, max = 255)
    private String description;

    @NotNull @Min(1)
    private BigDecimal amount;

    @NotNull
    private Boolean paid;
}
