package com.artivisi.training.microservice201902.invoice.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity @Data
public class Customer {

    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotEmpty @Size(min = 5, max = 100)
    @Column(name = "customer_number")
    private String number;

    @NotEmpty @Size(min = 3, max = 255)
    private String name;

    @NotEmpty @Size(min = 5, max = 255)
    @Email
    private String email;

    @NotEmpty @Size(min = 5, max = 100)
    private String mobilePhone;
}
