package com.artivisi.training.microservice201902.invoice.controller;

import com.artivisi.training.microservice201902.invoice.dao.CustomerDao;
import com.artivisi.training.microservice201902.invoice.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired private CustomerDao customerDao;

    @GetMapping("/")
    public Iterable<Customer> semuaCustomer() {
        return customerDao.findAll();
    }
}
